# ANPP

[![pipeline status](https://gitlab.com/Michael-F-Bryan/anpp/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/anpp/commits/master)

(**[Rendered Documentation]/[Benchmarks]**)

A Rust port of the *Advanced Navigation Packet Protocol*, a low overhead
communication protocol that can provide error checking and packetisation on top
of (possibly noisy) raw byte stream.

## Getting Started

To use this crate in your application you'll need to add it as a dependency in
your `Cargo.toml`.

```toml
# Cargo.toml

[dependencies]
anpp = { git = "https://gitlab.com/Michael-F-bryan/anpp" }
```

Then you can use it just like any other library:

```rust
extern crate anpp;

use anpp::{Decoder, Packet};
use anpp::errors::DecodeError;

fn decode_a_packet() -> Result<Packet, DecodeError> {
    // create the decoder and read some data into it
    let mut dec = Decoder::new();
    let data = read_some_data()?;
    dec.push_data(&data)?;

    // Now the decoder has some bytes to work with, we can decode the packet
    let pkt = dec.decode()?;

    Ok(pkt)
}
```

If you want to build this crate locally, first you'll need a copy of the source
code.

```console
$ git clone https://gitlab.com/Michael-F-Bryan/anpp
```

Then compile using the Rust build tool, `cargo`.

```console
$ cargo build
```

From here you can run the tests, generate HTML documentation, or execute the
benchmarks.

```console
$ cargo test
$ cargo doc --open
$ cargo bench
```


## Licensing

This port is released under either the Apache 2 or MIT license, at your
choosing.

The original *Advanced Navigation Packet Protocol* source code is available
under the MIT license and can be found on the *Advanced Navigation* website as
part of their Spatial SDK ([link]/[direct download]).

[Rendered Documentation]: https://michael-f-bryan.gitlab.io/anpp/crate-docs/index.html
[Benchmarks]: https://michael-f-bryan.gitlab.io/anpp/benchmarks/index.html
[link]: http://www.advancednavigation.com/product/spatial#software
[direct download]: http://www.advancednavigation.com/sites/default/files/product_software/spatialsdk.zip
