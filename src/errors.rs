//! All error types used in this crate.

use core::fmt::{self, Display, Formatter};
#[cfg(feature = "std")]
use std::error::Error;

/// There wasn't enough space left to complete the operation.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct InsufficientCapacity {
    /// The amount of space that was required.
    pub required: usize,
    /// The actual amount of space left.
    pub actual: usize,
}

#[cfg(feature = "std")]
impl Error for InsufficientCapacity {
    fn description(&self) -> &str { "Insufficient Capacity" }
}

impl Display for InsufficientCapacity {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "Insufficient capacity. At least {} bytes were required but only found {}",
            self.required, self.actual
        )
    }
}

/// An error encountered while decoding.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum DecodeError {
    /// More data is required.
    RequiresMoreData,
    /// A packet was found, but it had an invalid CRC.
    InvalidCRC,
}

impl DecodeError {
    fn msg(&self) -> &str {
        match *self {
            DecodeError::RequiresMoreData => "More data required",
            DecodeError::InvalidCRC => "Invalid CRC",
        }
    }
}

#[cfg(feature = "std")]
impl Error for DecodeError {
    fn description(&self) -> &str { self.msg() }
}

impl Display for DecodeError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result { self.msg().fmt(f) }
}

/// An error encountered while encoding a `Packet`.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum EncodeError {
    /// The buffer wasn't big enough.
    InsufficientCapacity(InsufficientCapacity),
    /// The packet was empty.
    EmptyPacket(EmptyPacket),
}

#[cfg(feature = "std")]
impl Error for EncodeError {
    fn description(&self) -> &str {
        match *self {
            EncodeError::InsufficientCapacity(ref i) => i.description(),
            EncodeError::EmptyPacket(ref e) => e.description(),
        }
    }
}

impl Display for EncodeError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            EncodeError::InsufficientCapacity(i) => i.fmt(f),
            EncodeError::EmptyPacket(e) => e.fmt(f),
        }
    }
}

impl From<InsufficientCapacity> for EncodeError {
    fn from(other: InsufficientCapacity) -> EncodeError {
        EncodeError::InsufficientCapacity(other)
    }
}

impl From<EmptyPacket> for EncodeError {
    fn from(other: EmptyPacket) -> EncodeError {
        EncodeError::EmptyPacket(other)
    }
}

/// The packet was empty.
///
/// All ANPP packets must contain at least one byte of data in order to be
/// transmitted.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct EmptyPacket;

impl EmptyPacket {
    fn msg(&self) -> &str { "Cannot encode an empty packet" }
}

#[cfg(feature = "std")]
impl Error for EmptyPacket {
    fn description(&self) -> &str { self.msg() }
}

impl Display for EmptyPacket {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result { self.msg().fmt(f) }
}
